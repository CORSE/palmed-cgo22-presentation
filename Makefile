TARGET=slides.pdf slides-short.pdf

AUTOGEN_SVGS_LIST=how_cpus_work resource_model supported palmed_struct
AUTOGEN_SVGS=$(addprefix img/_autogen/,$(AUTOGEN_SVGS_LIST))

QRCODE_SRC=$(wildcard img/*.qr.txt)
QRCODES=$(QRCODE_SRC:.qr.txt=.qr.pdf)

SVGS=$(wildcard img/*.svg)
SVG_PDFS=$(SVGS:.svg=.svg.pdf)

QRENCODE=qrencode --background=ffffff80 --margin 1 -t SVG

all: $(AUTOGEN_SVGS) $(SVG_PDFS) $(QRCODES) $(TARGET)

%.pdf: %.tex
	latexmk -pdfxe $<

img/_autogen/%: img/%.svg img/split_layers.py
	mkdir -p "$$(dirname "$@")"
	img/split_layers.py "$*.svg"
	cd "$@" ; inkscape --export-type=pdf *.svg

%.svg.pdf: %.svg
	inkscape "$<" -o "$@" 2>/dev/null

%.qr.pdf: %.qr.txt
	$(QRENCODE) -o "$(@:.pdf=.svg)" < $<
	inkscape "$(@:.pdf=.svg)" -o "$@" 2>/dev/null

.PHONY: clean
clean:
	rm -rf $(SVG_PDFS) img/_autogen
	latexmk -C

upload:
	scp slides.pdf palmed@palmed.corse.inria.fr:~/media/cgo22/cgo22-palmed-slides.pdf
