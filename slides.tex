% vim: spell spelllang=en

\documentclass[11pt,xcolor={usenames,dvipsnames},aspectratio=169]{beamer}
\usetheme{metropolis}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[absolute,overlay]{textpos}
\usepackage{tcolorbox}
\usepackage{texlib/my_listings}
\usepackage{texlib/todo}
\usepackage{xspace}
\usepackage{booktabs}
\usepackage{hyperref}
\usepackage{tabularx}
\usepackage{multirow}
\usepackage{xcolor,colortbl}

\newcommand{\na}{{\color{gray}N/A}}
\newcommand{\centerheader}[1]{\multicolumn{1}{c}{#1}}
\definecolor{taberrbg}{RGB}{254,219,255}
\definecolor{tabktaubg}{RGB}{215,250,205}
\newcolumntype{E}{>{\columncolor{taberrbg}}r}
\newcolumntype{K}{>{\columncolor{tabktaubg}}r}
\newcommand{\taberror}{\centerheader{Err.}}
\newcommand{\tabkendall}{\centerheader{$\tau_K$}}
\newcommand{\taberrorunit}{\centerheader{(\%)}}
\newcommand{\tabkendallunit}{\centerheader{$(1)$}}

\newcommand{\tool}{\textsc{Palmed}\xspace}

\definecolor{alertboxbg}{HTML}{ebb969}
\definecolor{alertboxborder}{HTML}{ff8f0f}

\newtcolorbox{alertbox}[1]{
    halign=center,
    valign=center,
    boxrule=1mm,
    fontupper=\bfseries\LARGE,
    colframe=alertboxborder,
    colback=alertboxbg,
    height=#1
}

\definecolor{largeboxbg}{HTML}{22373a}
\definecolor{largeboxborder}{HTML}{3c6367}
\newtcolorbox{largebox}{
    halign=center,
    valign=center,
    boxrule=1mm,
    fontupper=\LARGE,
    colframe=largeboxborder,
    colback=largeboxbg,
    coltext=white,
}

\newtcolorbox{eltbox}[1][]{
    valign=center,
    colback=alertboxbg,
    colframe=alertboxborder,
    fonttitle=\bfseries\large,
    #1
}

\newcommand{\alertoverbox}[1]{
    \begin{textblock*}{0.6\textwidth}[0.5,1](0.5\paperwidth,0.98\paperheight)%
        \begin{alertbox}{6em}
            #1
        \end{alertbox}
    \end{textblock*}
}

\addtobeamertemplate{background}{}{%
    \begin{textblock*}{3cm}[1,0](\paperwidth,0pt)%
        \colorbox{yellow}{
            \begin{minipage}[t][3cm][t]{\textwidth}
                \vspace{0.75cm}

                \centering\color{red}VIDEO \\
                HERE
            \end{minipage}
        }
    \end{textblock*}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{PALMED: Throughput Characterization for \newline
Superscalar Architectures}
\author[\slidecountline]
{
Nicolas~\textsc{Derumigny},
Théophile~\textsc{Bastian},
Fabian~\textsc{Gruber},
Guillaume~\textsc{Iooss},
Christophe~\textsc{Guillon},
Louis-Noël~\textsc{Pouchet},
Fabrice~\textsc{Rastello}
}
\date{}
%\subject{}
%\logo{}
\institute{
    \href{https://www.univ-grenoble-alpes.fr/}{Université Grenoble Alpes},
    \href{https://inria.fr}{INRIA},
    \href{https://st.com}{STMicroelectronics},
    \href{https://www.colostate.edu/}{Colorado State University}
}

\begin{document}

\maketitle{}

\begin{frame}{(Very) quick overview of \tool}
	\tool is:
	\begin{itemize}
		\item An automated framework...
		\item ... that reverse engineer CPUs...
		\item ... using only timing measurements
	\end{itemize}
	\tool outputs:
	\begin{itemize}
		\item A cost model for basic blocks of assembly instructions
	\end{itemize}
\end{frame}

\begin{frame}[fragile]{Scheduling instructions in a modern CPU}
    \vspace{1em}
    \begin{center}
    \hspace{-1em}
    \only<1>{\includegraphics[width=0.9\columnwidth]{img/_autogen/how_cpus_work/01.pdf}}%
    \only<2-3>{\includegraphics[width=0.9\columnwidth]{img/_autogen/how_cpus_work/02.pdf}}%
    \only<4>{\includegraphics[width=0.9\columnwidth]{img/_autogen/how_cpus_work/03.pdf}}%
    \only<5-7>{\includegraphics[width=0.9\columnwidth]{img/_autogen/how_cpus_work/04.pdf}}%
    \only<8>{\includegraphics[width=0.9\columnwidth]{img/_autogen/how_cpus_work/05.pdf}}%
    \only<9>{\includegraphics[width=0.9\columnwidth]{img/_autogen/how_cpus_work/06.pdf}}%
    \only<10->{\includegraphics[width=0.9\columnwidth]{img/_autogen/how_cpus_work/07.pdf}}%
    \end{center}

    \begin{center}
        \parbox[t][1.5em]{0.8\linewidth}{
            \begin{center}
                \LARGE
                \only<2-3>{\alert{Sequential} scheduling}
                \only<4>{\alert{Pipelined} scheduling}
                \only<5-7>{Luckily, CPUs are \alert{out-of-order}}
                \only<8>{CPUs have specialized execution ports}
                \only<9->{Scheduling with \alert{ports}}
            \end{center}
        }
    \end{center}

    \only<3>{\alertoverbox{Yeah. Well, maybe in 1200\,AD.}}
    \only<6-7>{\alertoverbox{More like it.\\\visible<7>{Still wrong, though.}}}
    \only<11-12>{\alertoverbox{Still incorrect.\\\visible<12>{But close enough.}}}
\end{frame}


%\begin{frame}{How to optimize high-performance binaries for these CPUs ?}
%\begin{itemize}
%\item Official documentation? -> not for all CPUs
%\item IACA, discontinued
%\item Manual try-and-error
%\item Relatively little ecosystem for high performance codes (TO CHECK)
%\end{itemize}
%\end{frame}

\begin{frame}{Can we predict execution time?}
    \alert{Complex}, but possible!

    Requires knowledge of:
    \begin{columns}
        \begin{column}[T]{0.4\textwidth}
            \begin{itemize}
                \item data dependencies
                \item memory accesses and caches
                \item branches taken and prediction
                \item \alert{ports used}
            \end{itemize}
        \end{column}
        \begin{column}[T]{0.4\textwidth}
            \begin{itemize}
                \visible<2->{\item[$\leftarrow$] Well studied}
                \visible<3->{\item[$\leftarrow$] Also well studied}
                \visible<4->{\item[$\leftarrow$] Behavior sufficiently known} % Cesnec + manipulation on Meltdown / spectre: good enough IMO
                \visible<5->{\item[$\leftarrow$] \textbf{Let's do this!}}
            \end{itemize}
        \end{column}
        \hfill
    \end{columns}

    \vspace{2em}
    \begin{center}
        \large
        Combining all those models $\leadsto$ better knowledge $\leadsto$
        better optimization
    \end{center}
\end{frame}

\begin{frame}{People did this before!}
    \begin{columns}
        \hspace{-0.80cm}
        \begin{column}[T]{0.365\textwidth}
            \begin{itemize}
                \item \textbf{Documentation:} designer
                \item \textbf{Manual work:} Agner Fog
                \item \textbf{HW counters:} uops.info
                \item \textbf{Designers' model:} IACA
                \item \textbf{ML:} Ithemal
                \item \textbf{Genetic alg.:} PMEvo
            \end{itemize}
        \end{column}
        \hspace{-1.1cm}
        \begin{column}[T]{0.58\textwidth}
            \begin{itemize}
                \visible<2->{\item[$\leftarrow$] incomplete / contains errors}
                \visible<3->{\item[$\leftarrow$] doesn't scale {(\small{}new CPUs)}, needs experts}
                \visible<4->{\item[$\leftarrow$] doesn't work everywhere, needs experts}
                \visible<5->{\item[$\leftarrow$] lots of expertise, proprietary}
                \visible<6->{\item[$\leftarrow$] blackbox, not informative enough}
                \visible<7->{\item[$\leftarrow$] automatic, doing great, but we're doing better}
            \end{itemize}
        \end{column}
        \hfill
    \end{columns}

    \vspace{1em}
    \begin{center}
	\visible<8->{
    All are \alert{relevant \& useful}, but either rely on \alert{expertise}
    or are \alert{not entirely satisfying}.

    \vspace{0.5em}
    }
    \end{center}
    \visible<9->{
    \begin{center}
        \Large
        \textbf{Goal:} \alert{fully automatic} approach, \alert{without HW
        counters}, that \alert{scales} on \alert{all CPUs}.
    \end{center}
    }
\end{frame}

\begin{frame}
    \vspace{7em}

    \begin{largebox}
        What's wrong with expertise?
    \end{largebox}

    \vspace{2em}

    \pause
    %\visible<2->
    {
        \large
    \begin{enumerate}
        \item We, experts, get bored of inferring figures each time a CPU gets
            released;\pause
        \item Too. freakingly. many. new. CPUs.
    \end{enumerate}
    }
\end{frame}

%\begin{frame}{Introduction: Yet another CPU performance model}
%General Idea:
%\begin{itemize}
%\item Optimizing code by hand is difficult
%\item Low-level CPU behavior is hard to model and ill-documented
%\item $\to$ \tool gives information about the interactions of assembly instructions by automated microbenchmarking
%\end{itemize}
%\end{frame}


\begin{frame}{Target: Superscalar Architectures (x86)}
	\begin{columns}
        \begin{column}[T]{0.60\textwidth}
            \begin{minipage}[t][\textheight][t]{\textwidth} % Force full height
                % to avoid awkward movement during animation
                \vfill
                \begin{center}
                    \only<1>{
                        \includegraphics[width=\textwidth]{img/rw_archs/intel-skylake.png}
                    }
                    \only<2>{
                        \includegraphics[width=\textwidth]{img/rw_archs/intel-sunny-cove.png}
                    }
                    \only<3>{
                        \includegraphics[width=\textwidth]{img/rw_archs/amd-zen3.png}
                    }
                    \only<4>{
                        \includegraphics[width=\textwidth]{img/rw_archs/intel-al-p-core.jpg}
                    }
                    \vfill
                \end{center}
                \vfill
            \end{minipage}
        \end{column}
		\begin{column}[T]{0.40\textwidth}
            \begin{center}
                \vspace{1.7cm}
                {\Large The number of ports is \alert{growing fast}:}

                \vspace{1em}

                \begin{tabular}{l l r}
                    \emph{CPU} & \emph{Year} & \emph{\#Ports} \\
                    \midrule
                    Intel \emph{Skylake} & 2015 & 8 \\
                    \pause{}Intel \emph{Ice Lake} & 2018 & 10 \\
                    \pause{}AMD \emph{Zen 3} & 2021 & 8+6 \\
                    \pause{}Intel \emph{Alder Lake} & 2021 & 12 \\
                \end{tabular}
            \end{center}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Target: Superscalar Architectures (ARM)}
	\begin{center}
		\includegraphics[width=0.7\textwidth]{img/rw_archs/arm-cortex-x2.png}
	\end{center}
	\begin{center}
        \vspace{-0.5em}
        {\large Cortex-X2: 6+4 ports}

        \vspace{0.6em}
        {\Large $\leadsto$ Even high-performance Arm CPUs use ports!}
	\end{center}
\end{frame}

\begin{frame}{Target: Superscalar Architectures (ARM) \#2}
However:
\begin{itemize}
\setlength{\itemsep}{1em}
\item Lots of (semi)-custom ARM designs (Apple, Qualcomm, Samsung, ...)
\item Growing influence on server chips (Ampere Computing)
\end{itemize}

\begin{itemize}
\setlength{\itemsep}{1em}
\item[$\to$] Not every one is properly documented...
\item[$\to$] ... hence an unoptimized toolchain
\end{itemize}
\end{frame}

\section*{Introducing the PALMED framework}

\begin{frame}
    \vspace{1em}
    \begin{center}
        \includegraphics[width=0.95\textwidth]{img/palmed_goal.svg.pdf}
    \end{center}
\end{frame}

\begin{frame}{Our target: basic block with no dependences}
    \vspace{1em}
    \begin{center}
    \only<1>{\includegraphics[width=0.95\textwidth]{img/_autogen/supported/01.pdf}}%
    \only<2>{\includegraphics[width=0.95\textwidth]{img/_autogen/supported/02.pdf}}%
    \only<3>{\includegraphics[width=0.95\textwidth]{img/_autogen/supported/03.pdf}}%
    \only<4>{\includegraphics[width=0.95\textwidth]{img/_autogen/supported/04.pdf}}%
    \end{center}
\end{frame}

\begin{frame}{Who will use this?}
\tool's cost model can be used by:
\begin{itemize}
\setlength{\itemsep}{1em}
\item \alert{Bottleneck analyzers} and \alert{optimization frameworks}: IACA, VTune, OSACA
\item \alert{Compilers}: code selection requires precise instruction behavior modeling
\item \alert{High performance library} programmers: when manual optimization of assembly kernels is needed
\end{itemize}
\end{frame}


\begin{frame}[fragile]{Our CPU model}
    \vspace{1em}
    \begin{center}
    \only<1>{\includegraphics[width=0.9\columnwidth]{img/_autogen/resource_model/01.pdf}}%
    \only<2>{\includegraphics[width=0.9\columnwidth]{img/_autogen/resource_model/02.pdf}}%
    \only<3>{\includegraphics[width=0.9\columnwidth]{img/_autogen/resource_model/03.pdf}}%
    \only<4>{\includegraphics[width=0.9\columnwidth]{img/_autogen/resource_model/04.pdf}}%
    \only<6>{\alertoverbox{Resources are not limited to ports!}}
    \only<5->{\includegraphics[width=0.9\columnwidth]{img/_autogen/resource_model/05.pdf}}%
    \end{center}
\end{frame}

\begin{frame}[fragile]{Disjunctive mappings}
	\includegraphics[width=0.8\columnwidth]{img/resource_graphs.svg.pdf}
    \only<2-3>{\alertoverbox{\texttt{add} uses R2 \emph{and} R3\visible<3>{\\ $\to$ \emph{conjunctive mapping}}}}
\end{frame}

\begin{frame}[fragile]{Conjunctive mapping}
	\begin{minipage}[t][0.4\textheight][t]{\textwidth}
		\vspace{4.5em}
		\only<1-2>{
    		\begin{largebox}
        		Real world is \emph{not} conjunctive...
    		\end{largebox}
    	}
   		\only<3->{
    		\begin{largebox}
        		Real world is \emph{not} conjunctive... but disjunctive!
    		\end{largebox}
    	}
    \end{minipage}
    
	\begin{minipage}[t][0.6\textheight][t]{\textwidth}
		\begin{center}
    		\only<2-3>{
    			\includegraphics[width=0.6\textwidth]{img/cpu-structure.svg.pdf}
    			\visible<3>{
    				\includegraphics[width=0.35\textwidth]{img/disjunctive.svg.pdf}
    			}
    		}
    	\end{center}
  		\only<4>{
    		\begin{itemize}
    			\item Need a \alert{schedule} to compute the execution time
    			\item Optimal schedule is an \alert{optimization problem} $\to$ costly
    			\item We proved that the conjunctive and disjunctive forms have the same expressiveness
    			\begin{itemize}
    				\item But our approach is not limited to ports
    			\end{itemize}
    		\end{itemize}
    	}
    \end{minipage}
\end{frame}

%\begin{frame}{Advantage of this representation}
%\begin{itemize}
%\item Conjunctive vs Disjunctive and the reduction of the computation complexity of the mapping problem.
%\item Not sure if this slide is useful or not, as we may loose our audience here. However, this is the corner stone of our contribution.
%\end{itemize}
%\end{frame}

\begin{frame}{How? Overview of \tool's internals}
	\vspace{2em}
	\only<1>{\includegraphics[width=\textwidth]{img/_autogen/palmed_struct/01.pdf}}
	\only<2>{\includegraphics[width=\textwidth]{img/_autogen/palmed_struct/02.pdf}}

	\begin{itemize}
		\only<1> {
			\vspace{-11pt} % because latex apparently wants a new line here?
			\item Micro-benchmark of \alert{every pair of instructions} (``quadratic benchmarks'')
			\item Collapse on \alert{equivalence classes} (hierarchical clustering)
			\item \alert{Two heuristics} to gather the Basic Instructions
		}
		\only<2> {
			\item \alert{Two-step solving} of the Basic Instruction
			\item Computation of a \alert{first benchmarks}
			\item Use part of the generated benchmarks to map the \alert{complete ISA}
		}
	\end{itemize}
\end{frame}

\begin{frame}{Evaluation}
\begin{itemize}
    \item Evaluated on BBs from \alert{SPEC'int} ($\sim{}10k$) and
        \alert{Polybench} ($\sim{}2k$)
    \item On \alert{Skylake-SP} (Intel) and \alert{ZEN1} (AMD)
%\item MSE: relative error
%\item Kendall's tau: ordering well conserved
%\item Values comparable to IACA on SKL-SP!
\end{itemize}

    \begin{center}
        \footnotesize
    \begin{tabular}{c c | E K | E K | E K | E K | E K}
        \toprule
        & & \multicolumn{2}{c}{\tool} & \multicolumn{2}{c}{uops.info} & \multicolumn{2}{c}{PMEvo} & \multicolumn{2}{c}{IACA} & \multicolumn{2}{c}{llvm-mca}\\
        & & \taberror & \tabkendall & \taberror & \tabkendall & \taberror & \tabkendall & \taberror & \tabkendall & \taberror & \tabkendall\\
        & Unit & \taberrorunit & \tabkendallunit & \taberrorunit & \tabkendallunit & \taberrorunit & \tabkendallunit & \taberrorunit & \tabkendallunit & \taberrorunit & \tabkendallunit\\
        \midrule
        % Generated from /home/tobast/src/palmed/results/data/scw-skx.2021-08-26.benchs.spec17
        \multirow{2}{*}{\textbf{SKL-SP}} & \textbf{SPEC2017} & 7.8 & 0.90 & 40.3 & 0.71 & 28.1 & 0.47 & 8.7 & 0.80 & 20.1 & 0.73\\
        % Generated from /home/tobast/src/palmed/results/data/scw-skx.2021-08-26.benchs.polybench
         & \textbf{Polybench} & 24.4 & 0.78 & 68.1 & 0.29 & 46.7 & 0.14 & 15.1 & 0.67 & 15.3 & 0.65\\
        % Generated from /home/tobast/src/palmed/results/data/scw-zen.2021-08-24.benchs.spec17
        \multirow{2}{*}{\textbf{ZEN1}} & \textbf{SPEC2017} & 29.9 & 0.68 & \na & \na & 36.5 & 0.43 & \na & \na & 33.4 & 0.75\\
        % Generated from /home/tobast/src/palmed/results/data/scw-zen.2021-08-24.benchs.polybench
         & \textbf{Polybench} & 32.6 & 0.46 & \na & \na & 38.5 & 0.11 & \na & \na & 28.6 & 0.40\\
        \bottomrule
    \end{tabular}
    \end{center}
\end{frame}

\begin{frame}{I want to try it out!}
    \begin{columns}
        \begin{column}[T]{0.47\textwidth}
            \begin{eltbox}[title=Source code,height=4cm]%
                \begin{minipage}[T]{0.72\textwidth}
                    \textbf{Source code available!}

                    \hfill \href{https://gitlab.inria.fr/nderumig/palmed}{git repo} $\rightarrow$
                \end{minipage}
                \hfill
                \begin{minipage}[T]{1.3cm}
                    \href{https://gitlab.inria.fr/nderumig/palmed}{\includegraphics[width=\textwidth]{img/gitlab_url.qr.pdf}}
                \end{minipage}

                \vfill

                \begin{itemize}
                    \item Python + Gurobi
                    \item GNU GPLv3
                \end{itemize}
            \end{eltbox}

            \vspace{0.5em}

            \begin{eltbox}[height=3.4cm,title=Docker image]%
                \begin{minipage}[T]{1.3cm}
                    \href{https://gitlab.inria.fr/CORSE/palmed-artifact}{\includegraphics[width=\textwidth]{img/artifact_url.qr.pdf}}
                \end{minipage}
                \hfill
                \begin{minipage}[T]{0.72\textwidth}
                    \hfill\textbf{Docker also available!}

                    \hspace{-0.3em}$\leftarrow$
                    \href{https://gitlab.inria.fr/CORSE/palmed-artifact}{there}
                \end{minipage}

                \vspace{2mm}
                Conference artifact, comes with \tool $\leadsto$ try it easily!
            \end{eltbox}
        \end{column}

        \begin{column}[T]{0.55\textwidth}
            \vspace{1.3cm}
            \begin{eltbox}[title=Try online!,height=5.7cm]%
                \begin{minipage}{0.7\textwidth}
                    \begin{itemize}
                        \item Input ASM code
                        \item Get resource usage
                    \end{itemize}
                \end{minipage}
                \hfill
                \begin{minipage}{1.3cm}
                    \href{https://palmed.corse.inria.fr}{\includegraphics[width=\textwidth]{img/webdemo.qr.pdf}}
                \end{minipage}

                \begin{center}
                    \small\url{https://palmed.corse.inria.fr/}
                \end{center}

                \begin{center}
                    \includegraphics[width=0.7\textwidth]{img/webdemo}
                \end{center}
            \end{eltbox}
        \end{column}
    \end{columns}
%\begin{itemize}
%\item Docker image available for easier integration
%\end{itemize}
\end{frame}

\begin{frame} %TAKE HOME MESSAGE, WORK HARD ON THIS
\vfill
\begin{center}
    \LARGE
    \tool is\ldots
\end{center}
\begin{itemize}
\setlength{\itemsep}{0.5em}
    \item A framework to \alert{model CPUs}
    \item Yielding a \alert{precise model} of instruction's interactions
    \item Fully \alert{automated}
    \item Requiring \alert{no hardware counters}, \alert{no expertise},
        \alert{no knowledge}
        \begin{itemize}
            \item ``Only'' requires the ISA semantics (``catalog of instructions
                available'')
        \end{itemize}
    \item Relying on a \alert{benchmark-generative approach}
    \item \alert{Able to scale} on large architectures
\end{itemize}
\end{frame}

\begin{frame}{Contact}
	Feel free to contact us at:
	\begin{itemize}
		\item \href{mailto:nicolas.derumigny@inria.fr}{nicolas.derumigny@inria.fr}
		\item \href{mailto:theophile.bastian@inria.fr}{theophile.bastian@inria.fr}
		\item \href{mailto:christophe.guillon@st.com}{christophe.guillon@st.com}
		\item \href{mailto:fabian.gruber@fadeopolis.com}{fabian.gruber@fadeopolis.com}
		\item \href{mailto:guillaume.iooss@inria.fr}{guillaume.iooss@inria.fr}
		\item \href{mailto:pouchet@colostate.edu}{pouchet@colostate.edu}
		\item \href{mailto:fabrice.rastello@inria.fr}{fabrice.rastello@inria.fr}
	\end{itemize}
	

\begin{minipage}{0.6\textwidth}
    \hfill\Large\textbf{Thank you!}
\end{minipage}
\hfill
\begin{minipage}{0.2\textwidth}
    \hfill{}Slides PDF $\rightarrow$
\end{minipage}
\begin{minipage}[T]{1.5cm}
    \href{https://palmed.corse.inria.fr/cgo22/cgo22-palmed-slides.pdf}{\includegraphics[width=\textwidth]{img/slides_url.qr.pdf}}
\end{minipage}
	
	\begin{center}
		\textit{This presentation and recording belong to the authors. No distribution 
	is allowed without the authors' permission.}
	\end{center}
\end{frame}

\end{document}
