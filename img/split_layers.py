#!/usr/bin/env python3

from xml.etree import ElementTree as ET
import sys
from pathlib import Path

IN_DIR = Path(__file__).parent
OUT_DIR = IN_DIR / "_autogen"

namespaces = {
    "svg": "http://www.w3.org/2000/svg",
    "inkscape": "http://www.inkscape.org/namespaces/inkscape",
}

splits = {
    "how_cpus_work.svg": {
        "_always": ["time_axis"],
        "01": ["ASM base"],
        "02": ["ASM base", "exec_linear"],
        "03": ["ASM base", "exec_pipeline"],
        "04": ["ASM reordered pipeline", "exec_pipeline reordered"],
        "05": [
            "ASM reordered pipeline",
            "exec_pipeline reordered",
            "ports",
            "ports key",
        ],
        "06": ["ASM reordered pipeline", "exec_ports", "ports", "ports key"],
        "07": [
            "ASM reordered ports",
            "exec_ports reordered",
            "ports reordered",
            "ports key",
        ],
    },
    "resource_model.svg": {
        "_always": ["Basic block", "ASM"],
        "01": [],
        "02": ["Usage", "Usage_graphical"],
        "03": ["Usage", "Usage_graphical", "Exec_time"],
        "04": ["Usage", "Usage_graphical", "Exec_time", "Bottlenecks"],
        "05": ["Usage", "Usage_graphical", "interference"],
    },
    "supported.svg": {
        "_always": ["ASM"],
        "01": [],
        "02": ["Basic block"],
        "03": ["Basic block", "Supports"],
        "04": ["Basic block", "Supports", "Unsupports"],
    },
    "palmed_struct.svg": {
        "_always": ["Basic_selection"],
        "01": ["mask_solve"],
        "02": ["Solving"],
    },
}


def process_svg(svg_splits, path, out_path):
    out_path.mkdir(parents=True, exist_ok=True)
    always = svg_splits.get("_always", [])

    svg = ET.parse(path.as_posix())

    for keyframe in svg_splits:
        if keyframe.startswith("_"):  # special value -- eg `_always`
            continue
        layers = always + svg_splits[keyframe]

        for layer_node in svg.findall(
            "./svg:g[@inkscape:groupmode='layer']", namespaces=namespaces
        ):
            layer_node.attrib["style"] = "display:none"
            print(
                "Layer: ",
                layer_node.attrib["{{{}}}{}".format(namespaces["inkscape"], "label")],
            )
        for layer_name in layers:
            layer = svg.find(
                ".//svg:g[@inkscape:groupmode='layer'][@inkscape:label='{}']".format(
                    layer_name
                ),
                namespaces=namespaces,
            )
            if not layer:
                raise Exception("Layer {} not found".format(layer_name))
            layer.attrib["style"] = "display:inline"

        keyframe_outpath = out_path / "{}.svg".format(keyframe)
        svg.write(keyframe_outpath.as_posix(), encoding="utf-8")
        print("Written {}".format(keyframe_outpath.as_posix()))


def main():
    processlist = []
    if len(sys.argv) < 2:
        processlist = splits.keys()
    else:
        processlist = [sys.argv[1]]

    for svg_path in processlist:
        process_svg(
            splits[svg_path], IN_DIR / svg_path, OUT_DIR / (Path(svg_path).stem)
        )


if __name__ == "__main__":
    main()
