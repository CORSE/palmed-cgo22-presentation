% vim: spell spelllang=en

\documentclass[11pt,xcolor={usenames,dvipsnames},aspectratio=169]{beamer}
\usetheme{metropolis}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[absolute,overlay]{textpos}
\usepackage{tcolorbox}
\usepackage{texlib/my_listings}
\usepackage{texlib/todo}
\usepackage{xspace}
\usepackage{booktabs}
\usepackage{hyperref}
\usepackage{tabularx}
\usepackage{multirow}
\usepackage{xcolor,colortbl}

\newcommand{\na}{{\color{gray}N/A}}
\newcommand{\centerheader}[1]{\multicolumn{1}{c}{#1}}
\definecolor{taberrbg}{RGB}{254,219,255}
\definecolor{tabktaubg}{RGB}{215,250,205}
\newcolumntype{E}{>{\columncolor{taberrbg}}r}
\newcolumntype{K}{>{\columncolor{tabktaubg}}r}
\newcommand{\taberror}{\centerheader{Err.}}
\newcommand{\tabkendall}{\centerheader{$\tau_K$}}
\newcommand{\taberrorunit}{\centerheader{(\%)}}
\newcommand{\tabkendallunit}{\centerheader{$(1)$}}

\newcommand{\tool}{\textsc{Palmed}\xspace}

\definecolor{alertboxbg}{HTML}{ebb969}
\definecolor{alertboxborder}{HTML}{ff8f0f}

\newtcolorbox{alertbox}[1]{
    halign=center,
    valign=center,
    boxrule=1mm,
    fontupper=\bfseries\LARGE,
    colframe=alertboxborder,
    colback=alertboxbg,
    height=#1
}

\definecolor{largeboxbg}{HTML}{22373a}
\definecolor{largeboxborder}{HTML}{3c6367}
\newtcolorbox{largebox}{
    halign=center,
    valign=center,
    boxrule=1mm,
    fontupper=\LARGE,
    colframe=largeboxborder,
    colback=largeboxbg,
    coltext=white,
}

\newtcolorbox{eltbox}[1][]{
    valign=center,
    colback=alertboxbg,
    colframe=alertboxborder,
    fonttitle=\bfseries\large,
    #1
}

\newcommand{\alertoverbox}[1]{
    \begin{textblock*}{0.6\textwidth}[0.5,1](0.5\paperwidth,0.98\paperheight)%
        \begin{alertbox}{6em}
            #1
        \end{alertbox}
    \end{textblock*}
}

\addtobeamertemplate{background}{}{%
    \begin{textblock*}{3cm}[1,0](\paperwidth,0pt)%
        \colorbox{yellow}{
            \begin{minipage}[t][3cm][t]{\textwidth}
                \vspace{0.75cm}

                \centering\color{red}VIDEO \\
                HERE
            \end{minipage}
        }
    \end{textblock*}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{PALMED: Throughput Characterization for \newline
Superscalar Architectures}
\author[\slidecountline]
{
Nicolas~\textsc{Derumigny},
Théophile~\textsc{Bastian},
Fabian~\textsc{Gruber},
Guillaume~\textsc{Iooss},
Christophe~\textsc{Guillon},
Louis-Noël~\textsc{Pouchet},
Fabrice~\textsc{Rastello}
}
\date{}
%\subject{}
%\logo{}
\institute{
    \href{https://www.univ-grenoble-alpes.fr/}{Université Grenoble Alpes},
    \href{https://inria.fr}{INRIA},
    \href{https://st.com}{STMicroelectronics},
    \href{https://www.colostate.edu/}{Colorado State University}
}

\begin{document}

\maketitle{}

\begin{frame}{Quick overview of \tool}
	\tool is:
	\begin{itemize}
		\item An automated framework...
		\item ... that reverse engineer CPUs...
		\item ... using only timing measurements
	\end{itemize}
	\tool outputs:
	\begin{itemize}
		\item A cost model for basic blocks of assembly instructions
	\end{itemize}
\end{frame}

\begin{frame}{Can we predict execution time?}
	\only<1-3>
	{
	Predict performance of a sequence of ASM instructions?\\
	\visible<2-3>
	{
	\includegraphics[width=0.9\columnwidth]{img/_autogen/resource_model/01.pdf}
	}
	\visible<3>
	{
	\\
    $\to$ \alert{Complex}, but possible!
	}
	}
	\only<4->
	{
    $\to$ \alert{Complex}, but possible!

    Requires knowledge of:
            \begin{itemize}
                \item data dependencies
                \item memory accesses and caches
                \item branches taken and prediction
                \item underlying topology of the execution units \alert{$\to$ ports used}
            \end{itemize}
        \hfill

    \vspace{2em}
    \begin{center}
        \large
        Combining all those models $\leadsto$ better knowledge $\leadsto$
        better optimization
    \end{center}
    }
\end{frame}

\begin{frame}
    \vspace{1em}
    \begin{center}
        \includegraphics[width=0.95\textwidth]{img/palmed_goal.svg.pdf}
    \end{center}
\end{frame}

\begin{frame}[fragile]{Our CPU model}
    \vspace{1em}
    \begin{center}
    \only<1>{\includegraphics[width=0.9\columnwidth]{img/_autogen/resource_model/01.pdf}}%
    \only<2>{\includegraphics[width=0.9\columnwidth]{img/_autogen/resource_model/02.pdf}}%
    \only<3>{\includegraphics[width=0.9\columnwidth]{img/_autogen/resource_model/03.pdf}}%
    \only<4>{\includegraphics[width=0.9\columnwidth]{img/_autogen/resource_model/04.pdf}}%
    \only<5->{\includegraphics[width=0.9\columnwidth]{img/_autogen/resource_model/05.pdf}}%
    \end{center}
\end{frame}

\begin{frame}{Who will use this?}
\tool's cost model can be used by:
\begin{itemize}
\setlength{\itemsep}{1em}
\item \alert{Bottleneck analyzers} and \alert{optimization frameworks}: IACA, VTune, OSACA
\item \alert{Compilers}: code selection requires precise instruction behavior modeling
\item \alert{High performance library} programmers: when manual optimization of assembly kernels is needed
\end{itemize}
\end{frame}

\begin{frame}{Evaluation}
\begin{itemize}
    \item Evaluated on BBs from \alert{SPEC'int} ($\sim{}10k$) and
        \alert{Polybench} ($\sim{}2k$)
    \item On \alert{Skylake-SP} (Intel) and \alert{ZEN1} (AMD)
%\item MSE: relative error
%\item Kendall's tau: ordering well conserved
%\item Values comparable to IACA on SKL-SP!
\end{itemize}

    \begin{center}
        \footnotesize
    \begin{tabular}{c c | E K | E K | E K | E K | E K}
        \toprule
        & & \multicolumn{2}{c}{\tool} & \multicolumn{2}{c}{uops.info} & \multicolumn{2}{c}{PMEvo} & \multicolumn{2}{c}{IACA} & \multicolumn{2}{c}{llvm-mca}\\
        & & \taberror & \tabkendall & \taberror & \tabkendall & \taberror & \tabkendall & \taberror & \tabkendall & \taberror & \tabkendall\\
        & Unit & \taberrorunit & \tabkendallunit & \taberrorunit & \tabkendallunit & \taberrorunit & \tabkendallunit & \taberrorunit & \tabkendallunit & \taberrorunit & \tabkendallunit\\
        \midrule
        % Generated from /home/tobast/src/palmed/results/data/scw-skx.2021-08-26.benchs.spec17
        \multirow{2}{*}{\textbf{SKL-SP}} & \textbf{SPEC2017} & 7.8 & 0.90 & 40.3 & 0.71 & 28.1 & 0.47 & 8.7 & 0.80 & 20.1 & 0.73\\
        % Generated from /home/tobast/src/palmed/results/data/scw-skx.2021-08-26.benchs.polybench
         & \textbf{Polybench} & 24.4 & 0.78 & 68.1 & 0.29 & 46.7 & 0.14 & 15.1 & 0.67 & 15.3 & 0.65\\
        % Generated from /home/tobast/src/palmed/results/data/scw-zen.2021-08-24.benchs.spec17
        \multirow{2}{*}{\textbf{ZEN1}} & \textbf{SPEC2017} & 29.9 & 0.68 & \na & \na & 36.5 & 0.43 & \na & \na & 33.4 & 0.75\\
        % Generated from /home/tobast/src/palmed/results/data/scw-zen.2021-08-24.benchs.polybench
         & \textbf{Polybench} & 32.6 & 0.46 & \na & \na & 38.5 & 0.11 & \na & \na & 28.6 & 0.40\\
        \bottomrule
    \end{tabular}
    \end{center}
\end{frame}


\begin{frame} %TAKE HOME MESSAGE, WORK HARD ON THIS
\vfill
\begin{center}
    \LARGE
    \tool is\ldots
\end{center}
\begin{itemize}
\setlength{\itemsep}{0.5em}
    \item A framework to \alert{model CPUs}
    \item Yielding a \alert{precise model} of instruction's interactions
    \item Fully \alert{automated}
    \item Requiring \alert{no hardware counters}, \alert{no expertise},
        \alert{no knowledge}
        \begin{itemize}
            \item ``Only'' requires the ISA semantics (``catalog of instructions
                available'')
        \end{itemize}
    \item Relying on a \alert{benchmark-generative approach}
    \item \alert{Able to scale} on large architectures
\end{itemize}
\end{frame}

\begin{frame}{Contact}
	Feel free to contact us at:
	\begin{itemize}
		\item \href{mailto:nicolas.derumigny@inria.fr}{nicolas.derumigny@inria.fr}
		\item \href{mailto:theophile.bastian@inria.fr}{theophile.bastian@inria.fr}
		\item \href{mailto:christophe.guillon@st.com}{christophe.guillon@st.com}
		\item \href{mailto:fabian.gruber@fadeopolis.com}{fabian.gruber@fadeopolis.com}
		\item \href{mailto:guillaume.iooss@inria.fr}{guillaume.iooss@inria.fr}
		\item \href{mailto:pouchet@colostate.edu}{pouchet@colostate.edu}
		\item \href{mailto:fabrice.rastello@inria.fr}{fabrice.rastello@inria.fr}
	\end{itemize}
	

\begin{minipage}{0.6\textwidth}
    \hfill\Large\textbf{Thank you!}
\end{minipage}
\hfill
\begin{minipage}{0.2\textwidth}
    \hfill{}Slides PDF $\rightarrow$
\end{minipage}
\begin{minipage}[T]{1.5cm}
    \href{https://palmed.corse.inria.fr/cgo22/cgo22-palmed-slides.pdf}{\includegraphics[width=\textwidth]{img/slides_url.qr.pdf}}
\end{minipage}
	
	\begin{center}
		\textit{This presentation and recording belong to the authors. No distribution 
	is allowed without the authors' permission.}
	\end{center}
\end{frame}

\end{document}
